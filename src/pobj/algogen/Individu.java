package pobj.algogen;
import java.util.Random;
 public class Individu implements Comparable<Individu>{
	private double valeurPropre;
	private double fitness;
	public Individu () {
		Random generator =new Random();
		
		this.fitness =0;
		
		double valPropre = generator.nextInt(10);
		this.valeurPropre = valPropre * 0.1 +valPropre * 0.5 ;
	}
	public Individu (double valeurPropre){
		this.valeurPropre =valeurPropre;
		this.fitness=0;
	}
	public String toString() { return "Valeur Propre : " +this.getValeurPropre()+" sa fitness est " +this.getFitness();
	}
	public void setFitness (double fitness){
		this.fitness = fitness;
	}
	public double getValeurPropre(){
		return this.valeurPropre;
	}
	public void setValeurPropre(double valeurPropre){
		this.valeurPropre = valeurPropre;
	}
	public double getFitness(){
	return this.fitness;
	}
	/**
	 * 
	 */
	@Override
	public int compareTo(Individu ind){
		if(this.getFitness()==ind.getFitness())
			return 0;
		if(this.getFitness()<ind.getFitness())
			return 1;
		else
			return -1;
		
	}
	@Override
	public Individu clone(){
		 Individu ind=new Individu();
		 ind.setFitness(this.getFitness());
		 ind.setValeurPropre(this.getValeurPropre());
		
		return ind;
			
		}
	/**
	 * 
	 */
	public void muter(){
		Random generator = new Random();
		double newValPropre=generator.nextInt(10);
		double p=this.getValeurPropre()+(0.05*this.getValeurPropre());
		double m=this.getValeurPropre()-(0.05*this.getValeurPropre());
		if(newValPropre<m && newValPropre>p)
			this.setValeurPropre(newValPropre);
		
		}
	/**
	 * 
	 */
	public Individu croiser(Individu ind){
		Individu newInd=new Individu((this.getValeurPropre()+ind.getValeurPropre())/2);
		newInd.setFitness((this.getFitness()+ind.getFitness())/2);
		return newInd;
		
	}
 }
	
