package pobj.algogen;
import java.lang.Math;
public class ValeurCible implements Environnement {
	private double cible;
	public ValeurCible (double cible){
		this.cible =cible;
	}
	public ValeurCible (){
		this.cible=Math.random();
	}
	
	public double eval(Individu i){
	//	Environnement env =new ValeurCible();
	double fitness =1/(Math.pow((this.getValue() - i.getValeurPropre()),2));
		i.setFitness(fitness);
		return i.getFitness();
	}
	public double getValue(){
		return  this.cible;
	}
	@Override
	public String toString (){
		return "valeur Cible= "+this.getValue();
	}
}
	

