package pobj.arith;

import java.util.Random;

public class ExpressionFactory {
	private static final int MAXVARIABLE=2;
	private static final Random generator= new Random();
	public static Expression createConstante(double value){
		return new Constante(value);
	}
	public static Expression createVariable(int rang){
		return new Variable(rang);
	}
	public static Expression createOperateurBinaire(Expression left,Expression rigth, Operator type){
		return new OperateurBinaire(left, rigth, type);
	}
	/**
	 * on definit une expression aleatoire de profondeur maximale 3
	 * @return
	 */
	public static Expression createRandomExpression(int profondeur){
		
		
		Expression exp=null;
		/**
		 * on test si la profondeur de l'expression est egale à 0
		 * on crée une constante comprise entre 0 et 1
		 * cette operation est delecate  faut beaucoup deprécaution pour eviter de boucler à l'infini 
		 * cette fonction doit encore etre amelioré pour traiter les cas tel que la division par 0
		 */
		
		if(profondeur==0)
		exp=createConstante(generator.nextDouble());
			int index =generator.nextInt(3);
		switch (index) {
			case 0:
				exp=createVariable(generator.nextInt(MAXVARIABLE));
				break;
				case 1:
					exp=createConstante(generator.nextDouble());
					break;
				case 2:
					exp=createOperateurBinaire(createRandomExpression(profondeur-1), createRandomExpression(profondeur-1), Operator.values()[generator.nextInt(3)]);
					break;

			}
		
		
		return exp;
		
	}
	public static EnvEval createRandomEnvEval(){
		return new EnvEval(MAXVARIABLE);
		
		
	}
}
